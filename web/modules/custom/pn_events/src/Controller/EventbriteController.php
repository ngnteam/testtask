<?php

namespace Drupal\pn_events\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Methods to get data from API
 */
class EventbriteController extends ControllerBase {

/**
 * Get Event data by id
 */
  public function getEvent($id) {
    $result = \Drupal::service('pn_events.eventbrite')->getEvent($id);

    return new JsonResponse($result);
  }

}