<?php

namespace Drupal\pn_events;

use GuzzleHttp\Exception\RequestException;

/**
 * Class EventbriteService.
 */
class EventbriteService {

  /**
   * Key to the Eventbrite API.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * URL of the Eventbrite API.
   *
   * @var string
   */
  protected $apiUrl;

  /**
   * HTTP Client
   */
  public $client;

  /**
   * Constructs a new EventbriteService object.
   */
  public function __construct() {
    $this->apiUrl = 'https://www.eventbriteapi.com/v3';
    $this->apiKey = \Drupal::config('pn_event.settings')->get('api_key');
    $this->client = \Drupal::httpClient();
  }

  /**
   * Get event data from API by event ID
   */
  public function getEvent($event_id) {
    try {
      $request = $this->client->get($this->apiUrl . "/events/$event_id/?token=" . $this->apiKey)->getBody();
      $result = json_decode($request->getContents(), true);
    }
    catch (RequestException $e) {
      $this->loggerFactory->error($e);
      $result = [
        'status' => FALSE,
        'data' => $e->getMessage(),
      ];
    }

    return $result;
  }

}