(function ($) {

  'use strict';

  Drupal.behaviors.sliderHome = {
    attach: function () {
        $('#block-events-content .homepage-articles').addClass('owl-carousel');
        // console.log('2222');
        $('#block-events-content .homepage-articles').once().owlCarousel({
          nav: true,
          navText: ["<img src='/themes/custom/pn_events/images/arrow_left.png'>", "<img src='/themes/custom/pn_events/images/arrow_right.png'>"],
          dots: false,
          slideBy: 1,
          items: 4,
          responsive:{
            0:{
              items:1,
            },
            600:{
              items:1
            },
            1000:{
              items:4
            },
          }
        });
      }
    };

} (jQuery));